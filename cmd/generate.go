package main

import (
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

const templateString = `package heroicons

templ {{ .FuncName }}() {
  {{ .SVG }}
}
`

func main() {
	tag := "v2.1.3"
	dir := "./tmp/heroicons/" + tag
	_, err := os.Stat(dir)
	if err != nil {
		if !os.IsNotExist(err) {
			log.Fatalf("error finding heroicons repo directory: %s", err.Error())
		}
		log.Println("Pulling heroicons @tag: " + tag)
		_, gitErr := git.PlainClone(dir, false, &git.CloneOptions{
			URL:           "https://github.com/tailwindlabs/heroicons",
			ReferenceName: plumbing.ReferenceName(tag),
			Progress:      os.Stdout,
		})
		if gitErr != nil {
			log.Fatalf("error cloning heroicons repo: %s", err.Error())
		}
	}

	files, err := getFilesFromDir(dir + "/src/24/outline")
	if err != nil {
		log.Fatalf("error getting files from dir: %s", err.Error())
	}

	outdir := "./heroicons"
	_, err = os.Stat(outdir)
	if err != nil {
		if !os.IsNotExist(err) {
			log.Fatalf("error finding heroicons output directory: %s", err.Error())
		}

		dirErr := os.MkdirAll(outdir, os.ModePerm)
		if dirErr != nil {
			log.Fatalf("error creating heroicons output directory: %s", err.Error())
		}
	}

	tmpl, err := template.New("templ").Parse(templateString)
	if err != nil {
		log.Fatalf("error parsing template string: %s", err.Error())
	}

	for _, f := range files {
		pathParts := strings.Split(f, "/")
		iconname := strings.Split(pathParts[len(pathParts)-1], ".")[0] + "-outline"
		filename := strings.ReplaceAll(iconname, "-", "_") + ".templ"

		b, err := os.ReadFile(f)
		if err != nil {
			log.Fatalf("error reading file file %s: %s", f, err.Error())
		}

		outfile, err := os.Create("./heroicons/" + filename)
		if err != nil {
			log.Fatalf("error creating file %s: %s", filename, err.Error())
		}
		defer outfile.Close()

		funcName := titleFromKebab(iconname)
		svg := string(strings.ReplaceAll(string(b), "\n", ""))
		reW := regexp.MustCompile(`width="\d+"`)
		svg = reW.ReplaceAllString(svg, `width="1em"`)
		reH := regexp.MustCompile(`height="\d+"`)
		svg = reH.ReplaceAllString(svg, `height="1em"`)

		tmplData := struct {
			FuncName string
			SVG      string
		}{
			FuncName: funcName,
			SVG:      svg,
		}

		err = tmpl.Execute(outfile, tmplData)
		if err != nil {
			log.Fatalf("error writing template to file: %s", err.Error())
		}
		log.Printf("Generated %s", filename)
	}
}

func getFilesFromDir(path string) ([]string, error) {
	var files []string
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}

// titleFromKebab converts a kebab-case string to TitleCase.
func titleFromKebab(str string) string {
	words := strings.Split(str, "-") // Split the string into words
	for i, word := range words {
		words[i] = strings.ToUpper(string(word[0])) + strings.ToLower(string(word[1:]))
	}
	return strings.Join(words, "") // Join the words back into a single string
}
