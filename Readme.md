# Templ Icons

Templ Icons is a modified mirror of Tailwind's Hero Icons converted into Templ templates. This package allows developers to utilize Hero Icons within their Templ templates.

## Installation

To use Templ Icons in your project, ensure you have Go installed and your project is initialized with Go Modules. Then, run the following command to add the package to your project:

```
go get gitlab.com/JankTech/templ-icons
```

## Usage

Import Templ Icons into a Templ template
You can then include an icon in your Templ template by calling the appropriate icon function. For example, to add an "arrow left" outline icon:

```
package main

import (
    "gitlab.com/JankTech/templ-icons/heroicons"
)

templ Component() {
  @heroicons.ArrowLeftOutline()
  <div>
    <!-- other markup -->
  </div>
}


```

## Documentation

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/JankTech/templ-icons.svg)](https://pkg.go.dev/gitlab.com/JankTech/templ-icons)

Full `go doc` style documentation for the package can be viewed online without
installing this package by using the GoDoc site here:
https://pkg.go.dev/gitlab.com/JankTech/templ-icons
